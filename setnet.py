import time
import network
import machine
import ubinascii
from umqttsimple import MQTTClient
import netconstants as nc # The "constants" to manage network connections
import logging, sys

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    ap_if = network.WLAN(network.AP_IF)
    if not sta_if.isconnected():
        logging.info('connecting to network...')
        sta_if.active(True)
        sta_if.connect(nc.essid, nc.wlanpass)
        ctime=time.time()
        while not sta_if.isconnected():
            if time.time()-ctime > 20:
                logging.warning('Network timeout')
                break
            time.sleep(0.5)
    msg='network config:' + str(sta_if.ifconfig())
    logging.info(msg)
    ap_if.active(False)


def restart_and_reconnect():
    logging.warning('Failed to connect to MQTT broker. Reconnecting...')
    time.sleep(10)
    machine.reset()
