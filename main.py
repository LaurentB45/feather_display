""" Main module for feather with OLED. """

import uasyncio as asyncio
import ubinascii
import network
from machine import Pin, I2C, RTC, unique_id
from ntptime import settime
import setnet as net
import ssd1306
import asynclib as aslib
from umqttsimple import MQTTClient
import time

import logging, sys

def sub_cb2 (topic, msg):
    global msg0, msg1, msg2
    logging.info('CB')
    logging.info(str(topic))
    logging.info(str(msg))
    piece=str(topic).split('/')[3]
    logging.info('piece ' + piece)
    grandeur=str(topic).split('/')[4].replace("'", '')
    logging.info("grandeur " + grandeur)
    valeur=str(msg).split("'")[1]+ ('C' if grandeur=='temp' else 'HPa')
    logging.info('moving messages')
    msg2=msg1
    msg1=msg0
    msg0= piece + ': ' + valeur 
    logging.info(msg0)
    oled.fill(0)
    oled.text(msg0, 0, 0)
    oled.text(msg1, 0, 10)
    oled.text(msg2, 0, 20)
    oled.show()


def connect_and_subscribe(client_id, mqtt_server, topic_sub, cbFn):
    client = MQTTClient(client_id, mqtt_server)
    client.set_callback(cbFn)
    client.connect()
    try:
        client.subscribe(topic_sub)
    except Exception as inst:
        print(type(inst))
        print(inst.args)
        print(inst)


    logging.info('Connected to %s MQTT broker, sub %s topic %s cb' % 
            (mqtt_server, topic_sub, cbFn))
    return client

def empty():
    """ useless function, called by run_every to flash the led"""


async def run_app_exit():
    """Infinite loop, stopped by the _stop_ button"""
    while runapp.value() == 1:
        client.check_msg()
        await asyncio.sleep(10)
    return

##############################################################################
# Main
##############################################################################
logging.basicConfig(stream=sys.stderr, level=logging.ERROR, format='%(asctime)s - %(levelname)s - %(message)s')
logging.debug('debug message...')
logging.info('info message')

runapp = Pin(12, Pin.IN, Pin.PULL_UP)
led = Pin(0, Pin.OUT)


if runapp.value() == 0:
    logging.info("arret application, Runapp=0")
else:
    logging.info("execution application, Runapp = 1")

logging.debug("Init mqtt")
client_id = ubinascii.hexlify(unique_id())
client = connect_and_subscribe(client_id,
                               nc.mqtt_server,
                               "/maison/#",
                               sub_cb2)

rtc = RTC()
rtc.datetime((2017, 8, 23, 1, 12, 48, 0, 0)) # set a specific date and time
rtc.datetime() # get date and time

settime()

now = rtc.datetime()

# ESP8266 Pin assignment
i2c = I2C(sda=Pin(4), scl=Pin(5), freq=20000)

OLED_WIDTH = 128
OLED_HEIGHT = 32
oled = ssd1306.SSD1306_I2C(OLED_WIDTH, OLED_HEIGHT, i2c)

oled.fill(0)
msg0='wait...'
oled.text(msg0, 0, 0)
msg1='----'
oled.text(msg1, 0, 10)
msg2='----'
oled.text(msg2, 0, 20)
oled.show()

loop = asyncio.get_event_loop()

logging.debug("creating task")
loop.create_task(aslib.run_every(empty, sec=30))
logging.debug("task created")

try:
    loop.run_until_complete(run_app_exit())
except Exception as e:
    print(e)
#    led_error(step=6)
