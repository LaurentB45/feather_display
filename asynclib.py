"""Asynchronous management module"""

import uasyncio as asyncio
from machine import Pin
import logging, sys


runapp = Pin(12, Pin.IN, Pin.PULL_UP)
led = Pin(0, Pin.OUT)


def empty():
    """ Empty function to be called simply to invoke run_every"""


async def run_every(function,  minute=1, sec=None):
    """ Function to run a task every interval"""
    debugmsg=("entered run_every parameters function "
          + "{fun} sec {sec} min {minute} ".
          format(fun=function, sec=sec, minute=minute))
    logging.debug(debugmsg)
    wait_sec = (sec if sec else minute * 60)
    while True:
        led.value(1)
        try:
            function()
        except Exception:
            msg="run_every catch exception for %s" % function
            logging.error(msg)
            raise
        await asyncio.sleep(0.1)
        led.value(0)
        await asyncio.sleep(wait_sec)


async def run_app_exit():
    """Infinite loop, stopped by the _stop_ button"""
    while runapp.value() == 1:
        await asyncio.sleep(10)
    return
